const express = require('express')
const app = express()
const cors = require('cors')

app.use(require('body-parser').json())
app.use(cors())

app.use('/', express.static('public'))

app.use('/controlApp', express.static('controlApp'))

app.use('/api', require('./actions'))

app.listen(process.env.PORT || 7000, () =>
  console.log(`port ${process.env.PORT || 7000}`)
)
