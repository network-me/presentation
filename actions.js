const app = require('express')()

let currentPage = 0
let canControlByPhone = false

app.get('/getPage', (req, res) => {
  res.status(200).send({ page: currentPage })
})

app.get('/setPage/:page', (req, res) => {
  const { page } = req.params
  console.log(page)
  currentPage = page
  res.status(200).send({ message: 'Page updated', page: currentPage })
})

app.get('/enableControl', (req, res) => {
  canControlByPhone = true
  return res
    .status(200)
    .send({ message: 'Controls enable', canControlByPhone: canControlByPhone })
})

app.get('/getControlStatus', (req, res) => {
  return res
    .status(200)
    .send({ message: 'Controls Stauts', canControlByPhone: canControlByPhone })
})

app.get('/disableControl', (req, res) => {
  canControlByPhone = false
  return res
    .status(200)
    .send({ message: 'Controls disable', canControlByPhone })
})

module.exports = app
