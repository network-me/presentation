const useLocalHost = window.location.href.includes('localhost')

const baseUrl = useLocalHost
  ? 'http://localhost:7000/api'
  : 'https://networkme-presentation.herokuapp.com/api'

let canControlByPhone = true
let currentPage = 0

// Pega página ao inicializar
const initialize = async () => {
  const { data } = await axios.get(`${baseUrl}/getPage`)
  setCurrentPage(+data.page)
}

// Pega página do servidor
const getServerPage = async () => {
  if (localStorage.getItem('controlIsEnable')) {
    const { data } = await axios.get(`${baseUrl}/getPage`)
    setCurrentPage(+data.page)
  }
}

const setCurrentPage = page => {
  if (canControlByPhone) {
    if (+page === 7) {
      window.location.hash = `#/fragments`
    } else {
      window.location.hash = `#/${page}`
    }
  }
}

// Coloca página na tela
const udatePageIdentifier = () => {
  let page = +window.location.hash.split('/')[1] + 1
  if (page === 14)
    setTimeout(
      () => (document.getElementById('questions').innerHTML = 'Perguntas?'),
      1500
    )
  if (page > 2) {
    document.getElementById('page').innerHTML = page // Number.isNaN(page) ? 7 : page;
  } else {
    if (Number.isNaN(page)) document.getElementById('page').innerHTML = '7'
    else document.getElementById('page').innerHTML = ''
  }
}

initialize()

setInterval(() => {
  udatePageIdentifier()
  getServerPage()
}, 500)
